<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210905070152 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE list_item ADD list_id INT NOT NULL');
        $this->addSql('ALTER TABLE list_item ADD CONSTRAINT FK_5AD5FAF73DAE168B FOREIGN KEY (list_id) REFERENCES to_do_list (id)');
        $this->addSql('CREATE INDEX IDX_5AD5FAF73DAE168B ON list_item (list_id)');
        $this->addSql('ALTER TABLE to_do_list ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE to_do_list ADD CONSTRAINT FK_4A6048ECA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_4A6048ECA76ED395 ON to_do_list (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE list_item DROP FOREIGN KEY FK_5AD5FAF73DAE168B');
        $this->addSql('DROP INDEX IDX_5AD5FAF73DAE168B ON list_item');
        $this->addSql('ALTER TABLE list_item DROP list_id');
        $this->addSql('ALTER TABLE to_do_list DROP FOREIGN KEY FK_4A6048ECA76ED395');
        $this->addSql('DROP INDEX IDX_4A6048ECA76ED395 ON to_do_list');
        $this->addSql('ALTER TABLE to_do_list DROP user_id');
    }
}

<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserDataPersister implements DataPersisterInterface
{
    private $userPasswordHasher;
    private $decoratedDataPersister;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher, DataPersisterInterface $decoratedDataPersister)
    {
        $this->userPasswordHasher = $userPasswordHasher;
        $this->decoratedDataPersister = $decoratedDataPersister;
    }

    /**
     * The persister system is similar with the voter system.
     * So multiple persister can exist and the appropriate one is selected using this method.
     *
     * @param $data - the data that is being persisted
     * @return bool if the persister supports this type of data
     */
    public function supports($data): bool
    {
        return $data instanceof User;
    }

    public function persist($data)
    {
        if ($data->getPlainPassword()) {
            // hash the plain password
            $data->setPassword(
                $this->userPasswordHasher->hashPassword($data, $data->getPlainPassword())
            );

            // erase the credentials that are temporarily stored
            $data->eraseCredentials();
        }
        $this->decoratedDataPersister->persist($data);
    }

    public function remove($data)
    {
        $this->decoratedDataPersister->remove($data);
    }
}